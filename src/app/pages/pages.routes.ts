import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { Rxjs2Component } from './rxjs2/rxjs2.component';
import { Rxjs3Component } from './rxjs3/rxjs3.component';
import { Rxjs4Component } from './rxjs4/rxjs4.component';



const pagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent, data: { titulo: 'Dashboard' } },
            { path: 'progress', component: ProgressComponent, data: { titulo: 'Barra de Progreso' } },
            { path: 'graficas1', component: Graficas1Component, data: { titulo: 'Graficas / Charts' } },
            { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Configuración de perfil' } },
            { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas' } },
            { path: 'rxjs', component: RxjsComponent, data: { titulo: 'Observable Parte I' } },
            { path: 'rxjs2', component: Rxjs2Component, data: { titulo: 'Observable Parte II' } },
            { path: 'rxjs3', component: Rxjs3Component, data: { titulo: 'Observable Parte III' } },
            { path: 'rxjs4', component: Rxjs4Component, data: { titulo: 'Observable Parte IV' } },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
        ]
    }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
