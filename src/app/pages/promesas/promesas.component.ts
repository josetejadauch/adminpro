import { Component, OnInit } from '@angular/core';
import { resolve } from '../../../../node_modules/@types/q';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: []
})
export class PromesasComponent implements OnInit {

  constructor() {

    this.contarTres().then(
      mensajeOK => console.log('Terminó!', mensajeOK)
    ).catch(
      error => console.error('Error en la promesa', error)
    );
  }

  ngOnInit() {
  }

  contarTres(): Promise<boolean> {
    return new Promise<boolean>( (resolve, reject) => {
      let contador = 0;
      let intervalo = setInterval( () => {
        contador++;
        console.log(contador);
        if ( contador === 3 ) {
          resolve(true);
          clearInterval(intervalo);
        }
      }, 1000);
    });
  }
}
