import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit {

  constructor() {

    this.regresaObservable().pipe(
      retry(2)
    ).subscribe(
      numero => console.log(numero),
      error => console.error('Error en el obs', error),
      () => console.log('El observador terminó')
    );
  }

  ngOnInit() {
  }

  regresaObservable(): Observable<number> {
    return new Observable( observer => {
      let contador = 0;
      let interval = setInterval(() => {
        contador++;
        observer.next( contador );
        if ( contador === 3 ) {
          clearInterval(interval);
          observer.complete();
        }
        if (contador === 2) {
          // clearInterval(interval);
          observer.error('Auxilio');
        }
      }, 1000);
    });
  }
}
