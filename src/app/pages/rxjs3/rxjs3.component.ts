import { Component, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs3',
  templateUrl: './rxjs3.component.html',
  styles: []
})
export class Rxjs3Component implements OnInit {

  constructor() {

    this.regresaObservable()
    .subscribe(
      numero => console.log(numero),
      error => console.error('Error en el obs', error),
      () => console.log('El observador terminó')
    );
  }

  ngOnInit() {
  }

  regresaObservable(): Observable<any> {
    return new Observable( (observer: Subscriber<any>) => {
      let contador = 0;
      let interval = setInterval(() => {
        contador++;

        const salida = {
          valor : contador
        };

        observer.next( salida );
        if ( contador === 3 ) {
          clearInterval(interval);
          observer.complete();
        }
        // if (contador === 2) {
          // clearInterval(interval);
          // observer.error('Auxilio');
        // }
      }, 1000);
    }).pipe(
      map( resp => resp.valor),
      filter( (valor, index) => {
        return valor % 2 === 1;
      })
    );
  }
}
