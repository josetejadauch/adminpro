import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs4',
  templateUrl: './rxjs4.component.html',
  styles: []
})
export class Rxjs4Component implements OnInit, OnDestroy {

  subscription: Subscription;

  constructor() {

    this.subscription = this.regresaObservable()
    .subscribe(
      numero => console.log(numero),
      error => console.error('Error en el obs', error),
      () => console.log('El observador terminó')
    );
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    console.log('La pagina se va a cerrar');
    this.subscription.unsubscribe();
  }

  regresaObservable(): Observable<any> {
    return new Observable( (observer: Subscriber<any>) => {
      let contador = 0;
      let interval = setInterval(() => {
        contador++;

        const salida = {
          valor : contador
        };

        observer.next( salida );
        // if ( contador === 3 ) {
        //   clearInterval(interval);
        //   observer.complete();
        // }
        // if (contador === 2) {
          // clearInterval(interval);
          // observer.error('Auxilio');
        // }
      }, 1000);
    }).pipe(
      map( resp => resp.valor),
      filter( (valor, index) => {
        return valor % 2 === 1;
      })
    );
  }
}
