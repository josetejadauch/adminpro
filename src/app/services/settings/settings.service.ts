import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  ajustes: Ajustes = {
    temaUrl: 'assets/css/colors/default.css',
    temaNombre: 'default'
  };

  constructor(@Inject(DOCUMENT) private _document) {
    this.cargarAjustes();
  }

  guardarAjustes() {
    localStorage.setItem('ajustes', JSON.stringify(this.ajustes));
  }

  cargarAjustes() {
    if ( localStorage.getItem('ajustes') ) {
      this.ajustes = JSON.parse(localStorage.getItem('ajustes'));
      console.log('Cargando settings de localStorage');
    } else {
      console.log('Usando valores por defecto');
    }
    this.aplicarTema(this.ajustes.temaNombre);
  }

  aplicarTema( tema: string ) {
    let url = `assets/css/colors/${tema}.css`;
    console.log(url);
    this._document.getElementById('tema').setAttribute('href', url );

    this.ajustes.temaNombre = tema;
    this.ajustes.temaUrl = url;

    this.guardarAjustes();
  }
}

interface Ajustes {
  temaUrl: string;
  temaNombre: string;
}
