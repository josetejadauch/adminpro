import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any = [
    {
      titulo: 'Principal',
      icono: 'mdi mdi-codepen',
      submenu: [
        { titulo: 'Dashboardx', url: '/dashboard' },
        { titulo: 'ProgressBar', url: '/progress' },
        { titulo: 'Gráficas', url: '/graficas1' },
        { titulo: 'Promesas', url: '/promesas' },
        { titulo: 'RXJS', url: '/rxjs' },
        { titulo: 'RXJS2 (Ejemplo usando MAP)', url: '/rxjs2' },
        { titulo: 'RXJS3 (Ejemplo usando FILTER)', url: '/rxjs3' },
        { titulo: 'RXJS4 (Ejemplo del Unsubscribe)', url: '/rxjs4' }
      ]
    }
  ];

  constructor() { }
}
